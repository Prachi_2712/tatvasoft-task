package com.example.tatavsofttask.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class PagginationScrollListner(var layoutManager:LinearLayoutManager):RecyclerView.OnScrollListener() {
    abstract fun isLoading():Boolean
    abstract fun isLastPage():Boolean
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
        val visibleItemCount = layoutManager.childCount
        val totalItemcount = layoutManager.itemCount

        if (!isLoading() && !isLastPage()) {
            if (firstVisibleItemPosition + visibleItemCount >= totalItemcount && firstVisibleItemPosition >= 0) {
                loadMoreData()
            }
        }

    }

    abstract fun loadMoreData()
}