package com.example.tatavsofttask.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.widget.Toast

object AppUtils {

    fun hasInternet(context: Context):Boolean{
        val connectiivtyManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val nw = connectiivtyManager.activeNetwork ?: return false
        val activeNetwork = connectiivtyManager.getNetworkCapabilities(nw) ?: return  false
        return when {
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }

    fun showToast(context: Context,mesage:String){
        Toast.makeText(context,mesage,Toast.LENGTH_LONG)
    }
}