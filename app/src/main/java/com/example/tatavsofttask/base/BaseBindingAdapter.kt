package com.example.tatavsofttask.base

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewParent
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

open abstract class BaseBindingAdapter<T>:RecyclerView.Adapter<BaseBindingViewHolder>() {
    protected var items: ArrayList<T> = ArrayList<T>()

    var isLoadingAdded = false

    override fun getItemCount() = items.count()

    fun setItem(items:ArrayList<T>){
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun clear(){
        val size = items.size
        if (items.isNotEmpty()){
            items.clear()
            notifyItemRangeRemoved(0,size)
        }
    }

    fun addItems(items:ArrayList<T>){
        val size = this.items.size
        this.items.addAll(items)
        val newSize = this.items.size
        notifyItemRangeInserted(size,newSize)
    }

    fun addLoading(item:T){
        isLoadingAdded = true
        this.items.add(item)
        notifyItemInserted(this.items.count() - 1)
    }

    fun removeLoading(){
        if (isLoadingAdded) {
            this.items.removeAt(this.items.count() - 1)
            notifyItemRemoved(this.items.count() - 1)
        }
        isLoadingAdded = false
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseBindingViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = bind(inflater,parent,viewType)
        return BaseBindingViewHolder(binding)
    }

    abstract fun bind(inflator:LayoutInflater,parent: ViewGroup,viewType:Int):ViewDataBinding
}