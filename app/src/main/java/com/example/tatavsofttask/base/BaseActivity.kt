package com.example.tatavsofttask.base

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.tatavsofttask.R
import com.example.tatavsofttask.utils.AppUtils
import java.lang.Exception

abstract open class BaseActivity<T:BaseViewModel>:AppCompatActivity() {
    abstract fun getViewmodel():T
    lateinit var viewModel: T

    private var progressDialog:Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setObservables()

    }

    private fun setObservables(){
        viewModel = getViewmodel()
        viewModel.errorMessage.observe(this,{
            AppUtils.showToast(this,resources.getString(it))
            onApiError()
        })
        viewModel.showLoading.observe(this ,{
            if (it){
                showLoading()
            } else {
                hideLoading()
            }
        })
    }

    private fun showLoading(){
        try {
            if (progressDialog == null){
                progressDialog = Dialog(this)
                progressDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            }

            val view = LayoutInflater.from(this).inflate(R.layout.app_loading_dialog,null,false)
            progressDialog?.setContentView(view)

            val window = progressDialog?.window
            window?.setBackgroundDrawable(ContextCompat.getDrawable(this,android.R.color.transparent))

            progressDialog?.setCancelable(false)
            progressDialog?.setCanceledOnTouchOutside(false)
            progressDialog?.show()
        }catch (e:Exception) {
            
        }

    }

    private fun hideLoading(){
        progressDialog?.run {
            if (isShowing) {
                progressDialog?.hide()
            }
        }
    }

    abstract fun onApiError()
}