package com.example.tatavsofttask.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.tatavsofttask.R
import com.example.tatavsofttask.network.ApiParams
import com.example.tatavsofttask.network.RxCallback
import com.example.tatavsofttask.utils.AppUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

open class BaseViewModel(application:Application) : AndroidViewModel(application) {

    var errorMessage:MutableLiveData<Int> = MutableLiveData()
    var showLoading:MutableLiveData<Boolean> = MutableLiveData()

    protected var subscription:Disposable? = null

    fun onApiStart(){
        showLoading.value = true
    }

    fun onApiFinish() {
        showLoading.value = false
    }

    fun <T> callRestApi (api:Observable<Response<T>>,rxCallback:RxCallback<T>?):Disposable? {
        return if (AppUtils.hasInternet(getApplication())) {
            (api)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe{onApiStart()}
                .doOnTerminate{onApiFinish()}
                .subscribe(
                    {handleSuccessResponse(it,rxCallback)},
                    this::handleError
                )
        } else {
            interNetConnectionerror()
            null
        }
    }

    fun <T> callRestApiWithoutProgress (api:Observable<Response<T>>,rxCallback:RxCallback<T>?):Disposable? {
        return if (AppUtils.hasInternet(getApplication())) {
            (api)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {handleSuccessResponse(it,rxCallback)},
                    this::handleError
                )
        } else {
            interNetConnectionerror()
            rxCallback?.onError()
            null
        }
    }

    private fun <T> handleSuccessResponse(response:Response<T>, rxCallBack:RxCallback<T>?){
        when (response.code()) {
            ApiParams.RESPONSE_CODE_SUCCESS_200 -> {
                rxCallBack?.onSuccess(response.body() as T)
            } else ->
                rxCallBack?.onError()

        }
    }

    fun handleError(error:Throwable){

    }

    fun interNetConnectionerror(){
        errorMessage.value = R.string.no_internet
    }
}