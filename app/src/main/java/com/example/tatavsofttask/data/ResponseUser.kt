package com.example.tatavsofttask.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseUser(
    val status:Boolean,
    val message:String?,
    val data:Data
):Parcelable {
    @Parcelize
    data class Data(
        val has_more:Boolean,
        val users:ArrayList<Users>
    ):Parcelable {
        @Parcelize
        data class Users(
            val name:String,
            val image:String,
            val items:ArrayList<String>?
        ):Parcelable {

        }
    }
}