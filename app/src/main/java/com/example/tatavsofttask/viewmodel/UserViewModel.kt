package com.example.tatavsofttask.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.tatavsofttask.base.BaseViewModel
import com.example.tatavsofttask.data.ResponseUser
import com.example.tatavsofttask.network.ApiParams
import com.example.tatavsofttask.network.ApiService
import com.example.tatavsofttask.network.RxCallback
import com.example.tatavsofttask.utils.Prefs

class UserViewModel(
    application:Application,
    val apiService: ApiService,
    val prefs: Prefs
): BaseViewModel(application) {
    private val userListResponse:MutableLiveData<ResponseUser> by lazy {
        MutableLiveData<ResponseUser>()
    }

    fun getUserListResponse():LiveData<ResponseUser> = userListResponse

    fun getUserList(pageNumber:Int,needToShowProgress:Boolean){
        val params = HashMap<String,String>()
        params[ApiParams.LIMIT] = 10.toString()
        params[ApiParams.OFFSET] = pageNumber.toString()
        if (needToShowProgress) {
            subscription = callRestApi(apiService.getUserList(params),
            object : RxCallback<ResponseUser> {
                override fun onSuccess(t: ResponseUser) {
                    handleUserResponse(t)
                }

                override fun onError() {

                }

            })
        } else {
            subscription = callRestApiWithoutProgress(apiService.getUserList(params),
            object : RxCallback<ResponseUser> {
                override fun onSuccess(t: ResponseUser) {
                    handleUserResponse(t)
                }

                override fun onError() {

                }

            })
        }
    }

    fun handleUserResponse(userResponse:ResponseUser){
        userListResponse.value = userResponse
    }
}