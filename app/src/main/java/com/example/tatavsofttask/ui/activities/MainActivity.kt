package com.example.tatavsofttask.ui.activities

import android.database.DatabaseUtils
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tatavsofttask.R
import com.example.tatavsofttask.base.BaseActivity
import com.example.tatavsofttask.data.ResponseUser
import com.example.tatavsofttask.databinding.ActivityMainBinding
import com.example.tatavsofttask.ui.adapter.UserAdapter
import com.example.tatavsofttask.utils.AppModules
import com.example.tatavsofttask.utils.PagginationScrollListner
import com.example.tatavsofttask.viewmodel.MyViewmodelFactory
import com.example.tatavsofttask.viewmodel.UserViewModel

class MainActivity : BaseActivity<UserViewModel>() {

    private var pageNumber = 0
    private lateinit var adapter : UserAdapter
    private var userList:ArrayList<ResponseUser.Data.Users> = ArrayList()

    private var isLastPage = false
    private var isLoading = false

    private val userViewModel: UserViewModel by viewModels<UserViewModel>{
        viewmodelFactory
    }
    lateinit var viewmodelFactory: MyViewmodelFactory

    lateinit var binding:ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        viewmodelFactory = MyViewmodelFactory(application,AppModules.provideApiService(),AppModules.providePrefs(this))
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        init()
    }

    private fun init(){
        binding.recUser.addOnScrollListener(object : PagginationScrollListner(binding.recUser.layoutManager as LinearLayoutManager) {
            override fun isLoading() = isLoading


            override fun isLastPage() = isLastPage

            override fun loadMoreData() {
                getUserList()
            }

        })
        binding.swpRefresh.setOnRefreshListener {
            pageNumber = 0
            getUserList()
        }
        adapter = UserAdapter()
        setObservables()
        getUserList()
    }

    private fun getUserList(){
        isLoading = true
        if (pageNumber > 0) {
            adapter.addLoading(ResponseUser.Data.Users("","",null))
            userViewModel.getUserList(pageNumber,false)
        } else {
            userViewModel.getUserList(pageNumber,true)
        }
    }

    private fun setObservables(){
        userViewModel.getUserListResponse().observe({ this.lifecycle }, {
            isLastPage = !(it.data.has_more)
            isLoading = false
            adapter.removeLoading()
            userList.addAll(it.data.users)
            binding.swpRefresh.isRefreshing = false
            if (pageNumber == 0) {
                adapter.clear()
                adapter.setItem(userList)
            } else {
                adapter.addItems(it.data.users)
            }
            binding.recUser.adapter = adapter
            pageNumber += 10
        })
    }

    override fun getViewmodel() = userViewModel
    override fun onApiError() {
        adapter.removeLoading()
        isLoading = false
    }

}