package com.example.tatavsofttask.ui.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TableRow
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.example.tatavsofttask.R
import com.example.tatavsofttask.base.BaseBindingAdapter
import com.example.tatavsofttask.base.BaseBindingViewHolder
import com.example.tatavsofttask.data.ResponseUser
import com.example.tatavsofttask.databinding.ItemUserBinding
import com.example.tatavsofttask.databinding.ItemUserLoadingBinding
import com.example.tatavsofttask.network.ApiParams

class UserAdapter: BaseBindingAdapter<ResponseUser.Data.Users>() {
    override fun bind(inflator: LayoutInflater, parent: ViewGroup, viewType: Int): ViewDataBinding {
        return if(viewType == ApiParams.VIEW_TYPE_LOADING){
            ItemUserLoadingBinding.inflate(inflator,parent,false)
        } else {
            ItemUserBinding.inflate(inflator,parent,false)
        }
    }

    override fun onBindViewHolder(holder: BaseBindingViewHolder, position: Int) {
        val binding = holder.binding
        if (binding is ItemUserBinding) {
            val item = items[position]
            binding.user = item
            Glide.with(binding.root.context)
                .load(item.image)
                .into(binding.imgUser)

            if (item.items?.isNullOrEmpty() == true) {
                binding.tbl.visibility = View.GONE
            } else {
                binding.tbl.visibility = View.VISIBLE

                var forLoopStartNumber = 0
                TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT)

                if (item.items.count() % 2 != 0) {
                    forLoopStartNumber = 1
                    val inflater =
                        binding.root.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val row =
                        inflater.inflate(R.layout.item_sing_image, null, false) as ConstraintLayout

                    if (row.getChildAt(0) is ImageView) {
                        val img = row.getChildAt(0) as ImageView
                        Glide.with(binding.root.context)
                            .load(item.items[0])
                            .into(img)
                    }
                    binding.tbl.addView(row,0)
                }

                var itemCount = forLoopStartNumber

                for (i in forLoopStartNumber..(item.items.count())/2) {
                    val inflater =
                        binding.root.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val row1 =
                        inflater.inflate(R.layout.item_double_image, null, false) as LinearLayout

                    if (row1.getChildAt(0) is ImageView) {
                        val img = row1.getChildAt(0) as ImageView
                        Glide.with(binding.root.context)
                            .load(item.items[itemCount])
                            .into(img)
                        itemCount += 1
                    }
                    if (row1.getChildAt(2) is ImageView) {
                        val img = row1.getChildAt(0) as ImageView
                        Glide.with(binding.root.context)
                            .load(item.items[itemCount])
                            .into(img)
                        itemCount += 1
                    }
                    binding.tbl.addView(row1,i)
                    if (itemCount >= item.items.count()) {
                        break
                    }
                }
                Log.e("Row count", binding.tbl.childCount.toString())
            }


        }
    }

    override fun getItemViewType(position: Int): Int {
        return if(isLoadingAdded && position == items.count() - 1) {
            return ApiParams.VIEW_TYPE_LOADING
        } else {
            return ApiParams.VIEW_TYPE_NOT_LOADING
        }
    }
}