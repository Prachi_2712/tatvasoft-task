package com.example.tatavsofttask.network

interface RxCallback<T> {
    fun onSuccess(t:T)
    fun onError()
}