package com.example.tatavsofttask.network

object ApiParams {
    const val BASE_URL = "https://sd2-hiring.herokuapp.com/api/"
    const val GET_USER_LIST = "users"
    const val RESPONSE_CODE_SUCCESS_200 = 200

    const val OFFSET = "offset"
    const val LIMIT = "limit"

    const val VIEW_TYPE_LOADING = 1
    const val VIEW_TYPE_NOT_LOADING = 2
}